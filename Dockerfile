FROM           centos 

RUN yum -y install httpd

ADD httpd.conf /etc/httpd/httpd.conf
RUN chmod 644 /etc/httpd/httpd.conf


EXPOSE 80 443


CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]

